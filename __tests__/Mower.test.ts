import { Mower } from '../src/classes/Mower';
import { iCoordinate, iMowerConfiguration } from '../src/classes/MowerConfigurationFile';

describe('Mower class ', () => {

  // Act before assertions
  beforeAll(async () => {



  });

  // Assert if setTimeout was called properly
  it('mower case 1', () => {
    let coordinates: iCoordinate = {
      x:5,
      y:5
    };
    let config: iMowerConfiguration = {
      coordinate: {
        x: 1,
        y: 2
      },
      direction: 'N',
      instructions: 'LFLFLFLFF'
    };
    let m = new Mower(config, coordinates);
    m.runMower();
    expect(m.getMowerState()).toBe('1 3 N');

  });
  it('mower case 2', () => {
    let coordinates: iCoordinate = {
      x:5,
      y:5
    };
    let config: iMowerConfiguration = {
      coordinate: {
        x: 3,
        y: 3
      },
      direction: 'E',
      instructions: 'FFRFFRFRRF'
    };
    let m = new Mower(config, coordinates);
    m.runMower();
    expect(m.getMowerState()).toBe('5 1 E');

  });
  it('mower case 3', () => {
    let coordinates: iCoordinate = {
      x:5,
      y:5
    };
    let config: iMowerConfiguration = {
      coordinate: {
        x: 0,
        y: 0
      },
      direction: 'N',
      instructions: 'FFFFFRRRRLLLL'
    };
    let m = new Mower(config, coordinates);
    m.runMower();
    expect(m.getMowerState()).toBe('0 5 N');

  });

  it('mower case - located outside of grid', () => {
    let coordinates: iCoordinate = {
      x:5,
      y:5
    };
    let config: iMowerConfiguration = {
      coordinate: {
        x: 7,
        y: 7
      },
      direction: 'N',
      instructions: 'FFFFFRRRRLLLL'
    };
    let m = new Mower(config, coordinates);
    m.runMower();
    expect(m.getMowerState()).toBe('7 7 N');

  });


});
