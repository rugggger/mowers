import * as fs from 'fs';
import { MowerConfigurationFile } from '../src/classes/MowerConfigurationFile';

jest.mock('fs');

describe('Configuration class ', () => {

  // Act before assertions
  beforeAll(async () => {


  });

  it('should throw error if not enough commands in file',  () => {

    // @ts-ignore
    fs.existsSync = jest.fn().mockReturnValue(true);

    let m = new MowerConfigurationFile("some_file");
    // @ts-ignore
    fs.readFileSync = jest.fn().mockReturnValue('5 5');
    expect(()=>{m.parseFile()}).toThrowError(new SyntaxError('Syntax Error: Bad syntax - not enough commands in file'));





  });

  it('should throw error if first line is corrupt', () => {
    let m = new MowerConfigurationFile("some_file");
    // @ts-ignore
    fs.readFileSync = jest.fn().mockReturnValue('5 l5\n5 5 M\nLFFF');
    expect(()=>{m.parseFile()})
      .toThrowError(new SyntaxError('Syntax Error: first line contains bad coordinates (5 l5)'));
  });

  it('should throw error if second line is corrupt', () => {
    let m = new MowerConfigurationFile("some_file");
    // @ts-ignore
    fs.readFileSync = jest.fn().mockReturnValue('5 5\n5 5 M\nLFFF');
    expect(()=>{m.parseFile()})
      .toThrowError(new SyntaxError('Syntax Error: bad mower coordinates in line 2: 5 5 M'));
  });

  it('should throw error if mower instructions are corrupt', () => {
    let m = new MowerConfigurationFile("some_file");
    // @ts-ignore
    fs.readFileSync = jest.fn().mockReturnValue('5 5\n5 5 N\nL4FFF');
    expect(()=>{m.parseFile()})
      .toThrowError(new SyntaxError('Syntax Error: bad mower instructions in line 3: L4FFF'));
  });

  it('should throw error if mower instruction are missing', () => {
    let m = new MowerConfigurationFile("some_file");
    // @ts-ignore
    fs.readFileSync = jest.fn().mockReturnValue('5 5\n5 5 N\nLFF\n3 3 W');
    expect(()=>{m.parseFile()})
      .toThrowError(new SyntaxError('Syntax Error: bad mower instructions in line 5: undefined'));
  });


});
