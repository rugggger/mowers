import * as fs from "fs";
import { MowersApp } from '../../src/classes/MowersApp';
jest.mock('fs');

describe('e2e test - read configuration and run mowers', () => {



  // Assert if setTimeout was called properly
  it('should return the right output for 2 mowers ', () => {
    // @ts-ignore
    fs.existsSync = jest.fn().mockReturnValue(true);
    // @ts-ignore
    fs.readFileSync = jest.fn().mockReturnValue('5 5\n1 2 N\nLFLFLFLFF\n3 3 E\nFFRFFRFRRF');
    let app = new MowersApp("somefile");
    app.run();
    expect(app.output).toBe("1 3 N\n5 1 E\n");
  });



});
