import { ConfigurationFile } from '../src/classes/ConfigurationFile';
import { FileError } from '../src/errors/errors';

describe('Configuration class ', () => {

  // Act before assertions
  beforeAll(async () => {



  });

   it('reads a configuration file', () => {
     const configFile = new ConfigurationFile("no_file");
     expect(()=>{configFile.parseFile()}).toThrowError(new FileError('File not found'));


  });



});
