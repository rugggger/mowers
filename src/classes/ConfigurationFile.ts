import * as fs from 'fs';
import * as path from 'path';
import { FileError } from '../errors/errors';

export class ConfigurationFile {
  filename:string;
  contents: string[];
  constructor(filename: string) {
    if (!filename) {
      this.filename =  path.join(__dirname,'../mower.config');
    }
     else {
      this.filename = filename;
    }

  }

   parseFile() {
    if (!fs.existsSync(this.filename)) {
      throw new FileError("File not found");
    }
    let contents;
    try {
      contents = fs.readFileSync(this.filename,'utf8').toString().split("\n");
    }
    catch (e) {
      console.log('error ',e);
    }


    //remove empty lines
     this.contents = contents.filter((line => line!=''));

  }
}


