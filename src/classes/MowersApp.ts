import { MowerConfigurationFile } from './MowerConfigurationFile';
import { Mower } from './Mower';


export class MowersApp {
  filename:string;
  output:string = '';
  constructor(filename:string) {
    this.filename = filename;
  }

  run(){
    try {
      const configFile = new MowerConfigurationFile(this.filename);
      configFile.parseFile();
      configFile.mowers.forEach((mowerConfig => {
        let m = new Mower(mowerConfig, configFile.upperRightCoordinate);
        m.runMower();
        this.output+= m.getMowerState() + "\n";
      }));
    }

    catch (e) {
      console.log(`${e.message}`);
    }
  }
}
