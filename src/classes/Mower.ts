import { direction, iCoordinate, iMowerConfiguration } from './MowerConfigurationFile';


export class Mower {
  mowerConfig: iMowerConfiguration;
  upperRightCoordinate: iCoordinate;
  constructor(mowerConfig: iMowerConfiguration, upperRightCoordinate: iCoordinate) {
    this.mowerConfig = mowerConfig;
    this.upperRightCoordinate = upperRightCoordinate;
  }
  changeDirection(turn: 'L'|'R'){
    switch (this.mowerConfig.direction) {
      case 'N':
        if (turn === 'L' ) {
          this.mowerConfig.direction = 'W';
        }
        if (turn === 'R') {
          this.mowerConfig.direction = 'E';
        }
        break;
      case 'E':
        if (turn === 'L' ) {
          this.mowerConfig.direction = 'N';
        }
        if (turn === 'R') {
          this.mowerConfig.direction = 'S';
        }
        break;
      case 'S':
        if (turn === 'L' ) {
          this.mowerConfig.direction = 'E';
        }
        if (turn === 'R') {
          this.mowerConfig.direction = 'W';
        }
        break;
      case 'W':
        if (turn === 'L' ) {
          this.mowerConfig.direction = 'S';
        }
        if (turn === 'R') {
          this.mowerConfig.direction = 'N';
        }
        break;
    }
  }
  moveForward(){
    switch (this.mowerConfig.direction) {
      case 'N':
        if (this.mowerConfig.coordinate.y < this.upperRightCoordinate.y) {
          this.mowerConfig.coordinate.y++;
        }
        break;
      case 'E':
        if (this.mowerConfig.coordinate.x < this.upperRightCoordinate.x) {
          this.mowerConfig.coordinate.x++;
        }
        break;
      case 'W':
        if (this.mowerConfig.coordinate.x > 0) {
          this.mowerConfig.coordinate.x--;
        }
        break;
      case 'S':
        if (this.mowerConfig.coordinate.y > 0) {
          this.mowerConfig.coordinate.y--;
        }
        break;
    }
  }
  getMowerState(){
    const state = `${this.mowerConfig.coordinate.x} ${this.mowerConfig.coordinate.y} ${this.mowerConfig.direction}`;
    return state;
  }
  runMower(){
    const instructions = this.mowerConfig.instructions.split('');
    instructions.forEach((instruction) => {
      switch (instruction) {
        case 'L':
        case 'R':
          this.changeDirection(instruction as ('L'|'R'));
          break;
        case 'F':
          this.moveForward();
          break;
      }
    });
  }

}
