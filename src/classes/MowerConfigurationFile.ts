import { SyntaxError } from '../errors/errors';
import { ConfigurationFile } from './ConfigurationFile';
import * as _ from 'lodash';

export type direction = 'N' | 'S' | 'W' | 'E';
export interface iCoordinate {
  x: number;
  y: number;
}
export interface iMowerConfiguration  {
  coordinate: iCoordinate;
  direction: direction;
  instructions: string;
}

export class MowerConfigurationFile extends ConfigurationFile {

  readonly coordinateRegEx = /^(\d+) (\d+)$/;
  readonly mowerCoordinateRegEx = /^(\d) (\d) ([N,E,S,W])$/;
  readonly mowerInstructionsRegEx = /^([R,L,F]+)$/;
  upperRightCoordinate: iCoordinate = {
    x:0,
    y:0
  };
  mowers: iMowerConfiguration[];



  parseFile() {
    super.parseFile();
    if (!this.contents || this.contents.length<2) {
      throw new SyntaxError("Bad syntax - not enough commands in file")
    }
    const coordinatesLine = this.contents[0];
    if (!this.coordinateRegEx.test(coordinatesLine)) {
      throw new SyntaxError(`first line contains bad coordinates (${coordinatesLine})`);
    }
    let match = this.coordinateRegEx.exec(coordinatesLine);
    this.upperRightCoordinate.x = parseInt(match[1]);
    this.upperRightCoordinate.y = parseInt(match[2]);

    const lines = this.contents.slice(1);
    const chunks = _.chunk(lines,2);

    this.mowers=[];
    chunks.forEach((chunk, index) => {
      if (!this.mowerCoordinateRegEx.test(chunk[0])) {
        const line = index*2+2; //line = chunk index * 2 + 2 lines at the beginning of the file
        throw new SyntaxError(`bad mower coordinates in line ${line}: ${chunk[0]}`);
      }
      if (!this.mowerInstructionsRegEx.test(chunk[1])) {
        const line = index*2+3; //line = chunk index * 2 + 2 lines at the beginning of the file + second line in chunk
        throw new SyntaxError(`bad mower instructions in line ${line}: ${chunk[1]}`);
      }


      const firstLine = this.mowerCoordinateRegEx.exec(chunk[0]);
      const secondLine = this.mowerInstructionsRegEx.exec(chunk[1]);
      const coordinate: iCoordinate = {
        x: parseInt(firstLine[1]),
        y: parseInt(firstLine[2])
      };
      const mower: iMowerConfiguration = {
        coordinate,
        direction: firstLine[3] as direction,
        instructions: secondLine[1],
      };
      this.mowers.push(mower);
    });

  }


}



