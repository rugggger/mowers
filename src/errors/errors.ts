
export class FileError extends Error {
  constructor(message) {
    super(message);
    this.message = `File Error: ${message}`;
  }

}


export class SyntaxError extends Error {
  constructor(message) {
    super(message);
    this.message = `Syntax Error: ${message}`;
  }
}
