import * as commandLineArgs from "command-line-args";
import { MowersApp } from './classes/MowersApp';


const optionDefinitions = [
  { name: 'src', type: String, multiple: false},
];


//
let options;
options = commandLineArgs(optionDefinitions);
let app = new MowersApp(options.src);
app.run();
console.log('Execution finished.');
console.log(app.output);



